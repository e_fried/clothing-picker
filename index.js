let httpClient = new HttpClient();

/* used to track user location */
class Position {
    constructor(latitude, longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}

/* Request the weather from Dark Sky API */
let requestWeatherData = function (position) {
    const root = "https://api.darksky.net/forecast";
    const apikey = "YOUR_API_KEY_HERE";
    const opts = "exclude=minutely,daily,flags,alerts"
    let latitude = position.latitude;
    let longitude = position.longitude;
    let url = root + "/" + apikey + "/" + latitude + "," + longitude + "?" + opts;
    httpClient.get(url, handleWeatherData);
}

/* Display current weather data and recommended clothing for forecast */
function handleWeatherData(weather) {
    weatherJSON = JSON.parse(weather);
    displayWeatherData(weatherJSON);
    recommendClothes(weatherJSON);
}

/* Use HTML5 Geolocation to find coordinates and request weather data */
function getCurrentLocationWeather() {
    if (isLocationAvailable()) {
        navigator.geolocation.getCurrentPosition(function (position) {
                let currPosition = new Position(position.coords.latitude, position.coords.longitude);
                requestWeatherData(currPosition);
            },
            function (error) {
                console.warn(`ERROR(${error.code}): ${error.message}`);
            }
        );
    }
    document.getElementById("address").value = "";
}

/* Use Google Maps API to find coordinates for address and request weather data */
function getWeatherForAddress() {
    let address = document.getElementById("address").value;
    const root = "https://maps.googleapis.com/maps/api/geocode/json?address=";
    const apiKey = "YOUR_API_KEY_HERE";
    let url = root + address + "&key=" + apiKey;

    if (address !== "") {
        httpClient.get(url, function (res) {
            res = JSON.parse(res);
            if (res.results[0] !== undefined) {
                let location = res.results[0].geometry.location;
                requestWeatherData(new Position(location.lat, location.lng));
            }
        });
    }
}

/* Display summary data to user and display Skycon */
function displayWeatherData(weather) {
    let skycons = new Skycons({
        "color": "black"
    });
    document.getElementById("summary").innerHTML = "Current weather: " + weather.currently.summary;
    document.getElementById("temperature").innerHTML = "Current temperature: " +
        weather.currently.temperature + " Fahrenheit";
    skycons.play();
    skycons.set("icon1", weather.currently.icon);
}

/* used to create a clothing rule and resulting image
if the rule is satisfied, return true, otherwise false */
class ClothingRule {
    constructor(check, image) {
        this.check = check;
        this.image = image;
    }
}

/* Rule saying to wear a windbreaker if warmer than 45 
and 15 or more mph wind */
let windbreakerRule = new ClothingRule(hours => {
    let res = false;
    hours.forEach(function (hour) {
        if (res !== true && hour.hasOwnProperty('windGust')) {
            if (hour.windGust >= 15 && hour.temperature > 45) {
                res = true;
            }
        }
    });
    return res;
}, "images/windbreaker.jpg")

/* Rule saying to bring an umbrella if it is going to rain
with a probability greater than 30% */
let umbrellaRule = new ClothingRule(hours => {
    let res = false;
    hours.forEach(function (hour) {
        if (res !== true && hour.hasOwnProperty('precipType')) {
            if (hour.precipType == 'rain' && hour.precipProbability >= 0.3) {
                res = true;
            }
        }
    })
    return res;
}, "images/umbrella.png");

/* Rule saying to wear a jacket if the weather is 45 or colder */
let winterJacketRule = new ClothingRule(hours => {
    let res = false;
    hours.forEach(function (hour) {
        if (res !== true) {
            if (hour.temperature <= 45) {
                res = true;
            }
        }
    });
    return res;
}, "images/winterjacket.jpg");

/* Calculate recommended clothing and display to user */
function recommendClothes(weather) {
    let hourlyForecasts = getHoursOfForecast(weather, 2);
    let rules = [windbreakerRule, umbrellaRule, winterJacketRule];
    let clothingDiv = document.getElementById('clothing');
    clothingDiv.innerHTML = "";
    rules.forEach(function (clothingRule) {
        if (clothingRule.check(hourlyForecasts)) {
            let image = "<img class=\"mx-3\" src=\"" + clothingRule.image + "\"height=\"100\" width=\"100\"> ";
            clothingDiv.innerHTML += image;
        }
    });
}

/* Get a subset of the hours to analyze for clothing */
function getHoursOfForecast(weather, hours) {
    return weather.hourly.data.slice(0, hours);
}

/* Function to tell if browser can determine location */
function isLocationAvailable() {
    return "geolocation" in navigator;
}