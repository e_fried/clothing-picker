# What is this?
This is a simple weather application that was created for Bring Your Kids to Work Day. The goal is to create a simple application where children can help decide what the suggested clothing would be based on the weather forecast. This is meant to be a functional version with some already completed clothing options.

## Getting Setup
1. Get a Dark Sky API key to for weather data requests
2. Get a Google Maps API key for location searches
3. Add CORS filter for Firefox. Geolocation API does not work reliably for Google Chrome